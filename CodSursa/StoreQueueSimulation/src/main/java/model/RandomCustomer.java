package model;

import java.util.ArrayList;
import java.util.Random;

public class RandomCustomer extends Thread {
    private static int count = 0;
    private int arrivalTimeMin;
    private int arrivalTimeMax;
    private int serviceTimeMin;
    private int serviceTimeMax;
    private int startTime;
    private ArrayList<CashRegister> cashRegisters;

    public RandomCustomer() {

    }

    public RandomCustomer(int arrivalTimeMin, int arrivalTimeMax, int serviceTimeMin, int serviceTimeMax,
                          ArrayList<CashRegister> cashRegisters, int startTime) {
        this.arrivalTimeMin = arrivalTimeMin;
        this.arrivalTimeMax = arrivalTimeMax;
        this.serviceTimeMin = serviceTimeMin;
        this.serviceTimeMax = serviceTimeMax;
        this.cashRegisters = cashRegisters;
        this.startTime = startTime;
    }

    @Override
    public void run() {
        while (true) {
            Random rand = new Random();
            int time = Util.getTime() - startTime;
            int randomNumber = rand.nextInt(serviceTimeMax) + serviceTimeMin;
            Customer customer = new Customer(count++, time, randomNumber);

            int min = 99999;
            int position = 0;
            for (int i = 0; i < cashRegisters.size(); i++) {
                if (min > cashRegisters.get(i).getWaitingTime()) {
                    position = i;
                    min = cashRegisters.get(i).getWaitingTime();
                }
            }
            int cashRegisterPosition = position;
            customer.setWaitingTime(min);
            cashRegisters.get(cashRegisterPosition).addCustomer(customer);
            EventsLogger.addEventLogger(new Message(time, "Arrived " + customer + "at the the cash register: " + cashRegisterPosition));
            System.out.println("Arrivied " + customer + "at the cash register: " + cashRegisterPosition);
            try {
                sleep((rand.nextInt(arrivalTimeMax) + arrivalTimeMin) * 1000);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        RandomCustomer.count = count;
    }

    public int getArrivalTimeMin() {
        return arrivalTimeMin;
    }

    public void setArrivalTimeMin(int arrivalTimeMin) {
        this.arrivalTimeMin = arrivalTimeMin;
    }

    public int getArrivalTimeMax() {
        return arrivalTimeMax;
    }

    public void setArrivalTimeMax(int arrivalTimeMax) {
        this.arrivalTimeMax = arrivalTimeMax;
    }

    public int getServiceTimeMin() {
        return serviceTimeMin;
    }

    public void setServiceTimeMin(int serviceTimeMin) {
        this.serviceTimeMin = serviceTimeMin;
    }

    public int getServiceTimeMax() {
        return serviceTimeMax;
    }

    public void setServiceTimeMax(int serviceTimeMax) {
        this.serviceTimeMax = serviceTimeMax;
    }

    public int getStartTime() {
        return startTime;
    }

    public void setStartTime(int startTime) {
        this.startTime = startTime;
    }

    public ArrayList<CashRegister> getCashRegisters() {
        return cashRegisters;
    }

    public void setCashRegisters(ArrayList<CashRegister> cashRegisters) {
        this.cashRegisters = cashRegisters;
    }

}
