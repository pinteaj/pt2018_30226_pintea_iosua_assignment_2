package model;

import java.util.*;

public class EventsLogger {
    private static List<Message> eventsLoggerList = new ArrayList<>();
    private static List<Map<Integer, Integer>> peakHoursMap = new ArrayList<>();

    public static void addEventLogger(Message message) {
        eventsLoggerList.add(message);
    }

    public static void clearEventsLogger() {
        eventsLoggerList.clear();
    }

    public static List<Message> getEventsLoggerList() {
        return eventsLoggerList;
    }

    public static int getPeakHour(int simulationTime) {
        int peakHour = 0;
        int maxNumberOfClients = -1;
        for (int i = 0; i <= simulationTime; i++) {
            int sum = 0;
            for (int j = 0; j < peakHoursMap.size(); j++) {
                sum += peakHoursMap.get(j).get(i);
            }
            if (sum > maxNumberOfClients) {
                maxNumberOfClients = sum;
                peakHour = i;
            }
        }
        return peakHour;
    }

    // for the event logger table
    public static Object[] getColumns() {
        Object[] columnNames = new String[peakHoursMap.size() + 1];
        columnNames[0] = "Time";
        for (int i = 1; i <= peakHoursMap.size(); i++) {
            columnNames[i] = "Cash register:" + (i - 1);
        }
        return columnNames;
    }

    public static Object[][] getData(int simulationTime) {
        Object[][] data = new Object[simulationTime + 1][peakHoursMap.size() + 1];
        for (int i = 0; i <= simulationTime; i++) {
            data[i][0] = i;
            for (int j = 0; j < peakHoursMap.size(); j++) {
                data[i][j + 1] = peakHoursMap.get(j).get(i);
            }
        }
        return data;
    }

    public static Object[] getEmptyTableColumns() {
        Object[] columnNames = new String[peakHoursMap.size()];
        for (int i = 0; i < peakHoursMap.size(); i++) {
            columnNames[i] = "Cash register:" + i;
        }
        return columnNames;
    }
    public static Object[][] getEmptyTableData(int simulationTime){
        Object[][] data = new Object[1][peakHoursMap.size()+1];
        for(int i = 0 ;i < peakHoursMap.size();i++){
            int sum = 0;
            for(int j = 0 ; j <= simulationTime;j++){
                if(peakHoursMap.get(i).get(j) != null && peakHoursMap.get(i).get(j) == 0){
                    sum++;
                }
            }
            data[0][i] = (double)sum/(simulationTime+1);
        }
        return data;
    }
    public static void setPeakHoursMap(List<Map<Integer, Integer>> peakHoursMaps) {
        peakHoursMap = peakHoursMaps;
    }

    public static void clearPeakHoursMap() {
        peakHoursMap.clear();
    }

    public static void addPeakHoursMap(Map<Integer, Integer> peakHour) {
        peakHoursMap.add(peakHour);
    }

    public static List<Map<Integer, Integer>> getPeakHoursMap() {
        return peakHoursMap;
    }

    public static String eventsLoggerToString() {
        Comparator<Message> comparator = new Comparator<Message>() {
            @Override
            public int compare(Message o1, Message o2) {
                return Integer.compare(o1.getTime(), o2.getTime());
            }
        };
        Collections.sort(eventsLoggerList, comparator);
        String returnedString = "";
        for (Message message : eventsLoggerList) {
            returnedString += message + "\n";
        }
        return returnedString;
    }
}
