package model;

public class Message {
    private int time;
    private String message;

    public Message(int time, String message) {
        this.time = time;
        this.message = message;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Message{" +
                "time=" + time +
                ", message='" + message + '\'' +
                '}';
    }
}
