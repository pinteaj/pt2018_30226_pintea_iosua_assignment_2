package model;

public class Util {
    public static final int CONV_NANO_TO_SEC = 1000000000;
    public static final int MAX_NUMBER_OF_QUEUES = 5;

    public static int getTime() {
        return (int) (System.nanoTime() / CONV_NANO_TO_SEC);
    }
}
