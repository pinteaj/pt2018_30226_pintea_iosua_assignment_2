package model;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CashRegister extends Thread {
    private int numberOfCustomers = 0;
    private int sumOfWaitingTime = 0;
    private int sumOfServiceTime = 0;
    private List<Customer> customers;
    private Map<Integer, Integer> peakHourMap;
    private int startTime;
    private Timer timer;

    public CashRegister(int startTime) {
        this.startTime = startTime;
        customers = new ArrayList<>();
        timer = new Timer(100, new TimerClass());
        this.peakHourMap = new HashMap<>();
    }

    @Override
    public void run() {
        while (true) {
            try {
                if (customers.size() == 0) {
                    sleep(100);
                } else {
                    sleep((this.customers.get(0).getServiceTime()) * 1000);
                    Customer customer = this.removeCustomer();
                    int curentTime = Util.getTime() - startTime;
                    customer.setFinishTime(curentTime);
                    System.out.println("Left the " + customer);
                    EventsLogger.addEventLogger(new Message(curentTime, "Left the " + customer));
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public synchronized void addCustomer(Customer customer) {
        numberOfCustomers++;
        customers.add(customer);
        this.sumOfWaitingTime += customer.getWaitingTime();
        this.sumOfServiceTime += customer.getServiceTime();
        notifyAll();
    }


    public synchronized Customer removeCustomer() {
        notifyAll();
        return customers.remove(0);
    }


    public int getWaitingTime() {
        int waitingTime = 0;
        if (customers.size() >= 1) {
            int curentTime = Util.getTime();
            waitingTime = customers.get(0).getArrivalTime() + customers.get(0).getServiceTime() - (curentTime - startTime);
            for (int i = 1; i < customers.size(); i++) {
                waitingTime += customers.get(i).getServiceTime();
            }
        }
        return waitingTime;
    }

    public void startTimer() {
        this.timer.start();
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public Map<Integer, Integer> getPeakHourMap() {
        return peakHourMap;
    }

    public void setPeakHourMap(Map<Integer, Integer> peakHourMap) {
        this.peakHourMap = peakHourMap;
    }

    public int getSumOfServiceTime() {
        return sumOfServiceTime;
    }

    public void setSumOfServiceTime(int sumOfServiceTime) {
        this.sumOfServiceTime = sumOfServiceTime;
    }

    public int getNumberOfCustomers() {
        return numberOfCustomers;
    }

    public void setNumberOfCustomers(int numberOfCustomers) {
        this.numberOfCustomers = numberOfCustomers;
    }

    public int getSumOfWaitingTime() {
        return sumOfWaitingTime;
    }

    public void setSumOfWaitingTime(int sumOfWaitingTime) {
        this.sumOfWaitingTime = sumOfWaitingTime;
    }

    public String toString() {
        return customers.toString();
    }

    public void stopTimer() {
        this.timer.stop();
    }


    //inner class
    private class TimerClass implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            peakHourMap.put(Util.getTime() - startTime, customers.size());
        }
    }
}
