package controller;

import model.CashRegister;
import model.EventsLogger;
import model.RandomCustomer;
import view.panel.OutputPanel;
import view.View;

import java.util.ArrayList;

public class TimeController extends Thread {

    private ArrayList<CashRegister> cashRegisters;
    private RandomCustomer randomCustomer;
    private int simulationTime;
    private View view;

    public TimeController(ArrayList<CashRegister> cashRegisters, RandomCustomer randomCustomer, int simulationTime, View view) {
        this.cashRegisters = cashRegisters;
        this.randomCustomer = randomCustomer;
        this.simulationTime = simulationTime;
        this.view = view;
    }

    @Override
    public void run() {
        try {
            sleep(simulationTime * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        randomCustomer.stop();
        for (CashRegister cashRegister : cashRegisters) {
            cashRegister.stop();
            cashRegister.stopTimer();
        }
        view.stopTimer();

        int numberOfCustomers = 0;
        int sumOfWaitingTime = 0;
        int sumOfServiceTime = 0;
        System.out.println("simulation is over");

        for(CashRegister cashRegister: cashRegisters){
            numberOfCustomers += cashRegister.getNumberOfCustomers();
            sumOfWaitingTime += cashRegister.getSumOfWaitingTime();
            sumOfServiceTime += cashRegister.getSumOfServiceTime();
            EventsLogger.addPeakHoursMap(cashRegister.getPeakHourMap());
        }
        int selectedOption = view.showOptionsPane();
        if(selectedOption == 0){
            double averageOfWaitingTime = (double)sumOfWaitingTime/numberOfCustomers;
            double averageOfSericeTime = (double)sumOfServiceTime/numberOfCustomers;
            OutputPanel outputPanel = new OutputPanel(simulationTime);
            outputPanel.setNumberOfCustomersJLabel("Total of customers was:" + numberOfCustomers);
            outputPanel.setAverageOfWaitingTimeJLabelText("Average of waiting time was:" + String.format("%.2f",averageOfWaitingTime));
            outputPanel.setAverageOfServiceTimeJLabelText("Average of service time was:" + String.format("%.2f",averageOfSericeTime));
            view.setOutputPanel(outputPanel);
            view.changeToOuputPanel();

        }
    }
}
