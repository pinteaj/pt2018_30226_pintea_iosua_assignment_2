package controller;

import model.CashRegister;
import model.EventsLogger;
import model.RandomCustomer;
import model.Util;
import view.View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;


public class Controller {
    private View view;
    private int arrivalTimeMin;
    private int arrivalTimeMax;
    private int serviceTimeMin;
    private int serviceTimeMax;
    private int numberOfQueues;
    private int simulationTime;
    private int startTime;
    private ArrayList<CashRegister> cashRegisters;
    private RandomCustomer randomCustomer;

    public Controller(View view) {
        this.view = view;
        this.cashRegisters = view.getCashRegisters();
        view.addStartSimulationButtonActionListener(new StartSimulationActionListener());
        view.addBackButtonActionListener(new BackActionListener());
    }


    private void startSimulation() {
        initializeSimulation();
        TimeController timeController = new TimeController(this.cashRegisters, this.randomCustomer, this.simulationTime,this.view);
        timeController.start();
        this.randomCustomer.start();
        for (CashRegister cashRegister : cashRegisters) {
            cashRegister.start();
        }

    }

    private void initializeSimulation() {
        this.startTime = Util.getTime();
        this.cashRegisters.clear();
        for (int i = 0; i < numberOfQueues; i++) {
            CashRegister cashRegister = new CashRegister(startTime);
            cashRegisters.add(cashRegister);
            cashRegister.startTimer();
        }
        this.view.startTimer();
        this.randomCustomer = new RandomCustomer(this.arrivalTimeMin, this.arrivalTimeMax, this.serviceTimeMin, this.serviceTimeMax, this.cashRegisters, this.startTime);
        RandomCustomer.setCount(0);
        EventsLogger.clearEventsLogger();
        EventsLogger.clearPeakHoursMap();
    }

    @Override
    public String toString() {
        return "Controller{" +
                "arrivalTimeMin=" + arrivalTimeMin +
                ", arrivalTimeMax=" + arrivalTimeMax +
                ", serviceTimeMin=" + serviceTimeMin +
                ", serviceTimeMax=" + serviceTimeMax +
                ", numberOfQueues=" + numberOfQueues +
                ", simulationTime=" + simulationTime +
                '}';
    }

    //innner classes
    private class StartSimulationActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                arrivalTimeMax = Integer.parseInt(view.getMaxArrivalTimeText());
                arrivalTimeMin = Integer.parseInt(view.getMinArrivalTimeText());
                serviceTimeMax = Integer.parseInt(view.getMaxServiceTimeText());
                serviceTimeMin = Integer.parseInt(view.getMinServiceTimeText());
                numberOfQueues = Integer.parseInt(view.getNumberOfQueuesText());
                simulationTime = Integer.parseInt(view.getSimulationTimeText());
                if(arrivalTimeMin > arrivalTimeMax){
                    throw new Exception("Arrival time max need to be greater than arrival time min");
                }else if(serviceTimeMin > serviceTimeMax){
                    throw new Exception("Service time max need to be greater than arrival time min");
                }else if(numberOfQueues > Util.MAX_NUMBER_OF_QUEUES){
                    throw  new Exception("Number of queues need to be smaller than " + Util.MAX_NUMBER_OF_QUEUES);
                }

                startSimulation();
            } catch (Exception ex) {
                view.showError(ex.getMessage());
            }


        }
    }

    private class BackActionListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            view.changeToMainPanel();
        }
    }
}
