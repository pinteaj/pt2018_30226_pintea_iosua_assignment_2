package view.panel;

import model.EventsLogger;

import javax.swing.*;
import java.awt.*;


public class OutputPanel extends JPanel {
    private JTextArea eventsLoggerJTextArea = new JTextArea();
    private JLabel numberOfCustomersJLabel = new JLabel();
    private JLabel averageOfWaitingTimeJLabel = new JLabel();
    private JLabel averageOfServiceTimeJLabel = new JLabel();
    private JTable tableOfQueueEvolution;
    private JTable tableOfAvgEmptyQueueTime;
    private int simulationTime;

    public OutputPanel(int simulationTime) {
        this.simulationTime = simulationTime;
        initializeComponents();
    }

    private void initializeComponents() {
        this.setLayout(new GridLayout(2, 0));

        JPanel firstPanel = new JPanel();
        firstPanel.setLayout(new GridLayout(0, 2));

        JPanel panelUp = new JPanel();
        panelUp.setLayout(new BoxLayout(panelUp, BoxLayout.Y_AXIS));

        tableOfAvgEmptyQueueTime = new JTable(EventsLogger.getEmptyTableData(simulationTime), EventsLogger.getEmptyTableColumns());
        JScrollPane scrollAVGEmpthQueueTime = new JScrollPane(this.tableOfAvgEmptyQueueTime);
        tableOfAvgEmptyQueueTime.setFillsViewportHeight(true);


        panelUp.add(numberOfCustomersJLabel);
        panelUp.add(averageOfWaitingTimeJLabel);
        panelUp.add(averageOfServiceTimeJLabel);
        panelUp.add(new JLabel("Peak hour was: " + EventsLogger.getPeakHour(simulationTime)));
        panelUp.add(new JLabel("Average of empty hour per cash register"));
        panelUp.add(scrollAVGEmpthQueueTime);


        this.tableOfQueueEvolution = new JTable(EventsLogger.getData(simulationTime), EventsLogger.getColumns());
        JScrollPane scrollTableOfQueueEvolution = new JScrollPane(this.tableOfQueueEvolution);
        scrollTableOfQueueEvolution.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        this.tableOfQueueEvolution.setFillsViewportHeight(true);

        firstPanel.add(panelUp);
        firstPanel.add(scrollTableOfQueueEvolution);


        this.eventsLoggerJTextArea.setColumns(70);
        this.eventsLoggerJTextArea.append(EventsLogger.eventsLoggerToString());
        this.eventsLoggerJTextArea.setEditable(false);
        this.eventsLoggerJTextArea.setPreferredSize(new Dimension(200, 100));

        JScrollPane scrollEventsLogger = new JScrollPane(eventsLoggerJTextArea);
        scrollEventsLogger.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollEventsLogger.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        this.add(firstPanel);
        this.add(scrollEventsLogger);
    }

    public void setAverageOfWaitingTimeJLabelText(String text) {
        this.averageOfWaitingTimeJLabel.setText(text);
    }

    public void setAverageOfServiceTimeJLabelText(String text) {
        this.averageOfServiceTimeJLabel.setText(text);
    }

    public void setNumberOfCustomersJLabel(String text) {
        this.numberOfCustomersJLabel.setText(text);
    }

    public String getEventsLoggerJTextAreaText() {
        return eventsLoggerJTextArea.getText();
    }

    public void setEventsLoggerJTextAreaText() {
        this.eventsLoggerJTextArea.append(EventsLogger.eventsLoggerToString());
    }
}
