package view.panel;

import model.CashRegister;
import model.Customer;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class QueuesEvolutionPanel extends JPanel {
    private ArrayList<CashRegister> cashRegisters;
    private int xPosition = 10;
    private int yPosition = 10;
    private Image cashRegisterImage;
    private Image customerImage;

    public QueuesEvolutionPanel(ArrayList<CashRegister> cashRegisters) {
        this.cashRegisters = cashRegisters;
        initializeImages();
    }

    private void initializeImages() {
        String path = System.getProperty("user.dir");
        try {
            cashRegisterImage = ImageIO.read(new File(path + "//images//cashRegister.png"));
            customerImage = ImageIO.read(new File(path + "//images//customer.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());

        if (cashRegisters != null) {
            for (CashRegister cashRegister : cashRegisters) {
                g.drawImage(cashRegisterImage,xPosition,yPosition,30,30,null);
                for (Customer customer : cashRegister.getCustomers()) {
                    xPosition += 50;
                    g.drawImage(customerImage,xPosition,yPosition,30,30,null);
                }
                xPosition = 10;
                yPosition += 50;
            }
        }
        xPosition = 10;
        yPosition = 10;
    }

}
