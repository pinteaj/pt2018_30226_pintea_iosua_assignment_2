package view;

import model.CashRegister;
import view.panel.OutputPanel;
import view.panel.QueuesEvolutionPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class View extends JFrame implements ActionListener {
    private JTextField minArrivalTimeJTextField = new JTextField();
    private JTextField maxArrivalTimeJTextField = new JTextField();
    private JTextField minServiceTimeJTextField = new JTextField();
    private JTextField maxServiceTimeJTextField = new JTextField();
    private JTextField numberOfQueuesJTextField = new JTextField();
    private JTextField simulationTimeJTextField = new JTextField();
    private JButton startSimulationButton = new JButton("Start simulation");
    private ArrayList<CashRegister> cashRegisters;
    private QueuesEvolutionPanel queuesEvolutionPanel;
    private OutputPanel outputPanel;
    private JButton backButton = new JButton("Back");
    private Timer timer = new Timer(10, this);

    public View(ArrayList<CashRegister> cashRegisterList) {
        this.cashRegisters = cashRegisterList;
        this.queuesEvolutionPanel = new QueuesEvolutionPanel(cashRegisters);
        initializeComponents();
    }

    public void startTimer() {
        timer.start();
    }

    public void stopTimer() {
        timer.stop();
    }

    private void initializeComponents() {
        this.setPreferredSize(new Dimension(1000, 300));
        this.setTitle("QueueSimulation");
        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setContentPane(mainPanel());
        this.pack();
    }

    private JPanel mainPanel() {
        JPanel mainPanel = new JPanel();
        JPanel selectPanel = new JPanel();
        selectPanel.setLayout(new GridLayout(5, 0));

        JPanel arrivalPanel = new JPanel();
        arrivalPanel.add(new JLabel("Set arrival time"));
        arrivalPanel.add(minArrivalTimeJTextField);
        arrivalPanel.add(maxArrivalTimeJTextField);

        JPanel servicePanel = new JPanel();
        servicePanel.add(new JLabel("Set service time"));
        servicePanel.add(minServiceTimeJTextField);
        servicePanel.add(maxServiceTimeJTextField);

        JPanel queuePanel = new JPanel();
        queuePanel.add(new JLabel("Set number of queues"));
        queuePanel.add(numberOfQueuesJTextField);

        JPanel simulationPanel = new JPanel();
        simulationPanel.add(new JLabel("Set simulation time"));
        simulationPanel.add(simulationTimeJTextField);
        selectPanel.add(arrivalPanel);
        selectPanel.add(servicePanel);
        selectPanel.add(queuePanel);
        selectPanel.add(simulationPanel);
        selectPanel.add(startSimulationButton);
        this.setComponentsPrefferedSize();


        mainPanel.setLayout(new GridLayout(0, 2));
        mainPanel.add(selectPanel);


        this.queuesEvolutionPanel.setPreferredSize(new Dimension(700, 400));
        JScrollPane scroller = new JScrollPane(this.queuesEvolutionPanel);
        mainPanel.add(scroller, BorderLayout.CENTER);
        return mainPanel;
    }

    private void setComponentsPrefferedSize() {
        minArrivalTimeJTextField.setPreferredSize(new Dimension(50, 25));
        maxArrivalTimeJTextField.setPreferredSize(new Dimension(50, 25));
        minServiceTimeJTextField.setPreferredSize(new Dimension(50, 25));
        maxServiceTimeJTextField.setPreferredSize(new Dimension(50, 25));
        numberOfQueuesJTextField.setPreferredSize(new Dimension(50, 25));
        simulationTimeJTextField.setPreferredSize(new Dimension(50, 25));
    }

    public OutputPanel getOutputPanel() {
        return outputPanel;
    }

    public void setOutputPanel(OutputPanel outputPanel) {
        this.outputPanel = outputPanel;
    }

    public String getMinArrivalTimeText() {
        return minArrivalTimeJTextField.getText();
    }

    public void setMinArrivalTimeText(String minArrivalTimeText) {
        this.minArrivalTimeJTextField.setText(minArrivalTimeText);
    }

    public String getMaxArrivalTimeText() {
        return maxArrivalTimeJTextField.getText();
    }

    public void setMaxArrivalTimeText(String maxArrivalTimeText) {
        this.maxArrivalTimeJTextField.setText(maxArrivalTimeText);
    }

    public String getMinServiceTimeText() {
        return minServiceTimeJTextField.getText();
    }

    public void setMinServiceTimeText(String minServiceTimeText) {
        this.minServiceTimeJTextField.setText(minServiceTimeText);
    }

    public String getMaxServiceTimeText() {
        return maxServiceTimeJTextField.getText();
    }

    public void setMaxServiceTimeText(String maxServiceTimeText) {
        this.maxServiceTimeJTextField.setText(maxServiceTimeText);
    }

    public String getNumberOfQueuesText() {
        return numberOfQueuesJTextField.getText();
    }

    public void setNumberOfQueuesText(String numberOfQueuesText) {
        this.numberOfQueuesJTextField.setText(numberOfQueuesText);
    }

    public String getSimulationTimeText() {
        return simulationTimeJTextField.getText();
    }

    public void setSimulationTimeText(String simulationTimeText) {
        this.simulationTimeJTextField.setText(simulationTimeText);
    }

    public ArrayList<CashRegister> getCashRegisters() {
        return cashRegisters;
    }

    public void setCashRegisters(ArrayList<CashRegister> cashRegisters) {
        this.cashRegisters = cashRegisters;
    }

    public void addStartSimulationButtonActionListener(ActionListener actionListener) {
        this.startSimulationButton.addActionListener(actionListener);
    }

    public void addBackButtonActionListener(ActionListener actionListener) {
        this.backButton.addActionListener(actionListener);
    }

    public void showError(String msg) {
        JOptionPane.showMessageDialog(this, msg, "Error", JOptionPane.ERROR_MESSAGE);
    }

    public int showOptionsPane() {
        Object[] options = {"Yes", "No"};
        return JOptionPane.showOptionDialog(this, "Press yes if you want to see the output results", "Output",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);

    }

    public void changeToOuputPanel() {
        this.setSize(new Dimension(1500, 1000));
        JPanel panel = new JPanel();
        panel.add(outputPanel);
        panel.add(backButton);
        this.setContentPane(panel);
        this.invalidate();
        this.validate();
        this.repaint();
    }

    public void changeToMainPanel() {
        this.setPreferredSize(new Dimension(1000, 300));
        this.setContentPane(mainPanel());
        this.pack();
        this.invalidate();
        this.validate();
        this.repaint();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.invalidate();
        this.validate();
        this.repaint();
        queuesEvolutionPanel.repaint();
    }


}
