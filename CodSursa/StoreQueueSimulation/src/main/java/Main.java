
import controller.Controller;
import model.CashRegister;
import view.View;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<CashRegister> cashRegisters = new ArrayList<>();
        View view = new View(cashRegisters);
        Controller controller = new Controller(view);
    }
}
